#!/usr/bin/env bash

docker run -it --rm --network host \
--ipc=host \
--gpus all \
-v /docker/cats:/workspace/cat-identifier \
fastdotai/fastai-dev:latest \
./run_jupyter.sh
